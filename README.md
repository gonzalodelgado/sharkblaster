# Shark Blaster

A simple cute 'em up for _A Game By Its Cover 2017_ game jam.  
Based on the [Shark Blaster famicase](http://famicase.com/17/softs/074.html):

![Shark Blaster famicase image](http://famicase.com/17/softs/074.jpg)


## Play it!

If finished, the game will be available on [itch.io](https://itch.io/) for free.  
In the meantime, the latest playable build is available here: http://gonzalodelgado.com/SharkBlaster/


## Hack it!

Game code is written using [HaxeFlixel](http://haxeflixel.com/).
You can install get started with HaxeFlixel by following
[its official guide](http://haxeflixel.com/documentation/getting-started/).

Once you're started with that, you can compile and run the game with this
command:

```
lime test neko
```

Levels are made using [Tiled](http://www.mapeditor.org/).  
More info about level-making soon :-)
