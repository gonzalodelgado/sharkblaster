package;

import flixel.FlxCamera.FlxCameraFollowStyle;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledLayer;
import flixel.addons.editors.tiled.TiledImageLayer;
import flixel.addons.editors.tiled.TiledTileSet;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.group.FlxSpriteGroup;
import flixel.system.FlxAssets;
import flixel.text.FlxText;
import flixel.tile.FlxTilemap;
import flixel.tweens.FlxTween;
import Enemies;

class PlayState extends FlxState
{
        private var _player:Player;
        private var _map:TiledLoader;
        private var _currentLevelNum:Int = 1;
        private var _level:FlxTilemap;
        private var _enemies:FlxSpriteGroup;
        public var chaser:FlxSprite;
        private var _levelText:FlxText;
        public inline static var SCROLLSPEED:Float = 16;
        private static var _enemyClassMap:Map<String,Class<Enemy>> = [
                "mecharacin" => Mecharacin,
                "octobot" => Octobot,
                "cyblowfish" => Cyblowfish,
                "mechrab" => Mechrab,
                "robodolphin" => Robodolphin,
        ];

        override public function create():Void
        {
                FlxG.mouse.visible = false;
                _player = new Player();
                _enemies = new FlxSpriteGroup();
                chaser = new FlxSprite();
                _levelText = new FlxText(0, 0, 0);
                _levelText.size = 64;
                reset();
                super.create();
        }

        public function reset():Void
        {
                clear();

                _map = new TiledLoader('assets/data/level$_currentLevelNum.tmx');
                _level = _map.loadTilemap("level");
                bgColor = _map.getBackgroundColor();
                add(_level);
                _enemies.clear();
                _map.loadEntities("enemies", initEnemy);
                _map.loadEntities("decorations");
                add(_enemies);

                _player.x = 10;
                _player.y = FlxG.height / 10;
                add(_player);
                add(_player.shots);

                _levelText.text = 'LEVEL $_currentLevelNum';
                _levelText.screenCenter();
                add(_levelText);
                FlxTween.tween(_levelText, {alpha: 0}, 2, {
                        startDelay: 2,
                        onComplete: _completeLevelTextTween,
                });
                chaser.x = 0;
                chaser.y = 0;
                chaser.width = FlxG.width;
                chaser.height = FlxG.height;
                chaser.visible = false;
                FlxG.camera.setScrollBoundsRect(_level.x,  _level.y, _level.width,  _level.height, true);
                FlxG.camera.target = chaser;
                FlxG.camera.style = FlxCameraFollowStyle.LOCKON;
                chaser.velocity.x = SCROLLSPEED;
                add(chaser);
        }

        private function _completeLevelTextTween(?tween:FlxTween):Void
        {
                remove(_levelText);
        }

        override public function update(elapsed:Float):Void
        {
                super.update(elapsed);
                FlxG.overlap(_player.shots, _enemies, null, _onEnemyShot);
                FlxG.overlap(_player, _enemies, null, _onEnemyCollision);
                FlxG.collide(_player, _level);

                /* go to next level when the end of this level is reached */
                /* NOTE: the reset call will be moved to a "boss defeated" event handler */
                if (_player.x >= _map.width - FlxG.width / 2)
                {
                        _currentLevelNum++;
                        reset();
                }
        }

        private function initEnemy(entity:TiledObject, ?simpleGraphic:FlxGraphicAsset):Void
        {
                if (_enemyClassMap.exists(entity.type))
                {
                        var enemy = Type.createInstance(_enemyClassMap[entity.type], [entity.x, entity.y - entity.height]);
                        enemy.loadGraphic(simpleGraphic, true, entity.width, entity.height);
                        enemy.loadAnimations();
                        enemy.health = entity.properties.contains('health') ? Std.parseFloat(entity.properties.get('health')) : 2;
                        enemy.damage = entity.properties.contains('damage') ? Std.parseFloat(entity.properties.get('damage')) : 1;
                        FlxG.log.notice('Created enemy $enemy with health ${enemy.health}');
                        _enemies.add(enemy);
                }
        }

        private function _onEnemyShot(shot:Shot, enemy:Enemy):Bool
        {
                if (enemy.isOnScreen())
                {
                        enemy.hurt(shot.damage);
                }
                if (!enemy.alive)
                {
                        _enemies.remove(enemy, true);
                        // TODO: update score or something
                }
                _player.shots.remove(shot, true);
                return false;
        }

        private function _onEnemyCollision(player:Player, enemy:Enemy):Bool
        {
                player.hurt(enemy.damage);
                // TODO: decrease player lives, switch to gameover state, etc.
                enemy.hurt(player.damage);
                return false;
        }
}
