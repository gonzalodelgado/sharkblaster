import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.input.gamepad.FlxGamepad;
import flixel.math.FlxMath;
import flixel.system.FlxAssets;
import flixel.util.FlxColor;

class Player extends FlxSprite
{
        public static inline var DEFAULT_XSPEED:Float = 48;
        public static inline var SPEED:Float = 192;
        public static inline var SHOTSPEED:Float = 528;
        private var _shotSprite:FlxSprite;
        public var shots:FlxSpriteGroup;
        public var damage:Float = 1;
        private var _gamepad:FlxGamepad;

        public function new(X:Float = 0, Y:Float = 0, ?SimpleGraphic:FlxGraphicAsset)
        {
                super(X, Y, SimpleGraphic);
                _gamepad= FlxG.gamepads.lastActive;
                _shotSprite = new FlxSprite();
                _shotSprite.loadGraphic(AssetPaths.blast__png, true, 9, 8);
                shots = new FlxSpriteGroup();
                velocity.x = DEFAULT_XSPEED;
                loadGraphic(AssetPaths.player__png, true, 48, 45);
                setFacingFlip(FlxObject.LEFT, true, false);
                setFacingFlip(FlxObject.RIGHT, false, false);
        }

        override public function update(elapsed:Float)
        {
                var up:Bool = false;
                var down:Bool = false;
                var left:Bool = false;
                var right:Bool = false;
                var shooting:Bool = false;
                velocity.set(DEFAULT_XSPEED * FlxMath.signOf(velocity.x), 0);
#if FLX_KEYBOARD
                up = FlxG.keys.anyPressed([UP, W]);
                down = FlxG.keys.anyPressed([DOWN, S]);
                left = FlxG.keys.anyPressed([LEFT, A]);
                right = FlxG.keys.anyPressed([RIGHT, D]);
                shooting = FlxG.keys.justPressed.SPACE;
#end
#if FLX_GAMEPAD
                if (_gamepad != null)
                {
                        up = up || _gamepad.pressed.DPAD_UP;
                        down = down || _gamepad.pressed.DPAD_DOWN;
                        left = left || _gamepad.pressed.DPAD_LEFT;
                        right = right || _gamepad.pressed.DPAD_RIGHT;
                        shooting = shooting || _gamepad.justPressed.A || _gamepad.justPressed.B || _gamepad.justPressed.X || _gamepad.justPressed.Y;
                }
#end
                /* TODO: touch controls for mobile */
                if (up)
                        moveUp();
                else if (down)
                        moveDown();
                if (left)
                        moveLeft();
                else if (right)
                        moveRight();
                _checkBoundaries();
                if (shooting)
                        shoot();
                super.update(elapsed);
        }

        private function _checkBoundaries()
        {
                var state:PlayState = cast FlxG.state;
                if (y < 0)
                        y = 0;
                if (x < state.chaser.x)
                        x = state.chaser.x;
                if (y + height > FlxG.height)
                        y = FlxG.height - height;
                if (x + width > state.chaser.x + state.chaser.width)
                        x = state.chaser.x + state.chaser.width - width;
        }

        public function moveUp():Void
        {
                velocity.y = -SPEED;
        }
        
        public function moveDown():Void
        {
                velocity.y = SPEED;
        }

        public function moveLeft():Void
        {
                velocity.x = -SPEED;
                facing = FlxObject.LEFT;
        }

        public function moveRight():Void
        {
                velocity.x = SPEED;
                facing = FlxObject.RIGHT;
        }

        public function shoot():Void
        {
                var shot = new Shot(flipX ? x : x + width, y + height / 2);
                shot.loadGraphicFromSprite(_shotSprite);
                shot.velocity.x = SHOTSPEED * (flipX ? -1 : 1);
                shots.add(shot);
        }
}
