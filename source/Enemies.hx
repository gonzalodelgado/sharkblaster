package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.system.FlxAssets;
import flixel.tweens.FlxTween;
import flixel.tweens.FlxEase;
import flixel.util.FlxColor;

class Enemy extends FlxSprite
{
        public var damage:Float = 1;

        override public function update(elapsed:Float):Void
        {
                if (alive && isOnScreen())
                        action(elapsed);
                super.update(elapsed);
        }

        private function _superHurt(Damage:Float)
        {
                super.hurt(Damage);
        }

        override public function hurt(Damage:Float):Void
        {
                function onCompleteHurtTween(tween:FlxTween):Void
                {
                        setColorTransform();
                        _superHurt(Damage);
                }
                FlxTween.color(this, 0.08, FlxColor.WHITE, FlxColor.RED, {onComplete: onCompleteHurtTween});
        }

        public function action(elapsed:Float):Void
        {
        }

        public function loadAnimations():Void
        {
        }

}

class Mecharacin extends Enemy
{
        override public function loadAnimations():Void
        {
                super.loadAnimations();
                animation.add("swim", [0, 1, 1, 2, 3, 3, 3, 3, 3, 3, 3, 3, 2, 0], 6, false);
        }

        override public function action(elapsed:Float):Void
        {
                var swimAnimation = animation.getByName("swim");
                super.action(elapsed);
                if (swimAnimation.finished)
                        animation.play("swim");
                if (swimAnimation.curIndex == 3)
                        velocity.x = -80;
                else if (swimAnimation.curIndex == 2) {
                        velocity.x = 0;
                }
                else {
                        velocity.x = -16;
                }
        }
}

class Cyblowfish extends Mecharacin
{
}

class Mechrab extends Enemy
{
        override public function loadAnimations():Void
        {
                super.loadAnimations();
                animation.add("walk", [0, 1, 2, 3], 6, true);
                animation.play("walk");
        }
        override public function action(elapsed:Float):Void
        {
                super.action(elapsed);
                velocity.x = -12;
        }
}

class Octobot extends Enemy
{
        private var _initialY:Float;
        private var _tween:FlxTween;
        private var _tweenCreated:Bool;

        public function new(X:Float=0, Y:Float=0, ?SimpleGraphic:FlxGraphicAsset)
        {
                super(X, Y, SimpleGraphic);
                _initialY = y;
                _tweenCreated = false;
        }

        private function _completeTween(tween:FlxTween):Void
        {
                velocity.y = 16;
        }

        override public function action(elapsed:Float)
        {
                super.action(elapsed);
                if (!_tweenCreated)
                {
                        _tween = FlxTween.tween(this, {y: y - 114}, 1, {
                                type: FlxTween.PERSIST,
                                ease: FlxEase.quadOut,
                                onComplete: _completeTween
                        });
                        _tweenCreated = true;
                }
                if (y >= _initialY)
                        _tween.start();
        }
}

class RobodolphinShot extends FlxSprite
{
        public function new(X:Float=0, Y:Float=0, ?SimpleGraphic:FlxGraphicAsset)
        {
                super(X, Y, SimpleGraphic);
                if (SimpleGraphic == null)
                {
                        x = x + 9;
                        y = y + 4;
                        loadGraphic(AssetPaths.robodolphinshot__png, true, 11, 9);
                }
                animation.add('shoot', [0, 1, 2, 3], 24, false);
                animation.play('shoot');
                animation.finishCallback = finishCallback;
        }

        public function finishCallback(name:String):Void
        {
                FlxG.state.remove(this);
                var bullet:FlxSprite = new FlxSprite(x, y);
                bullet.loadGraphic(AssetPaths.robodolphinbullet__png, true, 7, 6);
                bullet.velocity.x = -38;
                FlxG.state.add(bullet);
                destroy();
        }
}

class Robodolphin extends Enemy
{
        public var shotDelay:Int;
        public function new(X:Float=0, Y:Float=0, ?SimpleGraphic:FlxGraphicAsset)
        {
                super(X, Y, SimpleGraphic);
                velocity.x = -10;
                shotDelay = 100;
        }

        override public function action(elapsed:Float)
        {
                super.action(elapsed);
                if (--shotDelay <= 0)
                {
                        shotDelay = 100;
                        _shoot();
                }
        }

        private function _shoot():Void
        {
                var shot:RobodolphinShot = new RobodolphinShot(x, y);
                shot.velocity.x = velocity.x;
                FlxG.state.add(shot);
        }
}
