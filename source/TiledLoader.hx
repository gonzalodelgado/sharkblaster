package;
import haxe.io.Path;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.addons.editors.tiled.TiledMap;
import flixel.addons.editors.tiled.TiledObject;
import flixel.addons.editors.tiled.TiledObjectLayer;
import flixel.addons.editors.tiled.TiledTileLayer;
import flixel.addons.editors.tiled.TiledTileSet;
import flixel.system.FlxAssets;
import flixel.tile.FlxTilemap;
import flixel.util.FlxColor;
import openfl.Assets;

class TiledLoader
{
        private var _map:TiledMap;
        private var _tileLayer:TiledTileLayer;
        public var width:Float;
        public var height:Float;
        public static inline var TILESHEET_PATH:String = 'assets/data/';

        public function new(LevelData:String)
        {
                _map = new TiledMap(Xml.parse(Assets.getText(LevelData)));
                width = _map.fullWidth;
                height = _map.fullHeight;
        }

        public function getBackgroundColor():FlxColor
        {
                return _map.backgroundColor;
        }

        /* FIXME: for some reason the returned FlxTilemap width is 0 */
        public function loadTilemap(LayerName:String="tiles", TileSetName:String="tiles", ?customTileWidth:Int, ?customTileHeight:Int):FlxTilemap
        {
                var tileSet:TiledTileSet = _map.tilesets[TileSetName];
                var tileLayer:TiledTileLayer = cast _map.getLayer(LayerName);
                // TODO: check if layer is null and display error message
                var tileMap:FlxTilemap = new FlxTilemap();
                tileMap.loadMapFromCSV(
                                tileLayer.csvData,
                                Path.normalize(TILESHEET_PATH + tileSet.imageSource),
                                customTileWidth == null ? tileSet.tileWidth : customTileWidth,
                                customTileHeight == null ? tileSet.tileHeight : customTileHeight,
                                OFF,
                                tileSet.firstGID);
                return tileMap;
        }

        private function _drawEntity(entity:TiledObject, ?simpleGraphic:FlxGraphicAsset):Void
        {
                FlxG.state.add(new FlxSprite(entity.x, entity.y - entity.height, simpleGraphic));
        }

	public function loadEntities(EntityLayer:String = "entities", ?EntityLoadCallback:TiledObject->FlxGraphicAsset->Void):Void
        {
                var objectLayer:TiledObjectLayer = cast _map.getLayer(EntityLayer);
                var tileSet:TiledTileSet = objectLayer.map.tilesets[EntityLayer];
                if (EntityLoadCallback == null)
                        EntityLoadCallback = _drawEntity;
                for (object in objectLayer.objects)
                {
                        var image = tileSet.getImageSourceByGid(object.gid);
                        if (image == null) {
                                FlxG.log.warn('Failed to obtain image for ${object.gid}');
                                continue;
                        }
                        EntityLoadCallback(object, Path.normalize(TILESHEET_PATH + image.source));
                }
        }
}
