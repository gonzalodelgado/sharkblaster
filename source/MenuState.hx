package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.input.gamepad.FlxGamepad;
import flixel.text.FlxText;
import flixel.util.FlxColor;

class MenuState extends FlxState
{
        private var _startText:FlxText;
        private var _titleText:FlxText;

        override public function create():Void
        {
                FlxG.mouse.visible = false;
                bgColor = FlxColor.BLUE;
                _startText = new FlxText(0, 0, 0, 'PRESS START', 16);
                _startText.screenCenter();
                _startText.y = FlxG.height - FlxG.height / 3;
                add(_startText);
                _titleText = new FlxText(0, 0, 0, 'SHARK BLASTER');
                _titleText.setFormat("assets/fonts/ComicRelief-Bold.ttf", 32, FlxColor.WHITE, CENTER);
                _titleText.setBorderStyle(OUTLINE, FlxColor.MAGENTA, 2);
                _titleText.scale.set(2, 2.5);
                _titleText.screenCenter();
                _titleText.y = FlxG.height / 5;
                _titleText.y = 112;
                add(_titleText);
        }

        override public function update(elapsed:Float):Void
        {
                super.update(elapsed);
                var start:Bool = false;
#if FLX_KEYBOARD
                start = FlxG.keys.anyPressed([SPACE, ENTER]);
#end
#if FLX_GAMEPAD
                var gamepad:FlxGamepad = FlxG.gamepads.lastActive;
                if (gamepad != null)
                        start = start || gamepad.justPressed.START || gamepad.justPressed.A || gamepad.justPressed.B || gamepad.justPressed.X || gamepad.justPressed.Y;
#end
                if (start)
                        FlxG.switchState(new PlayState());
        }
}
